##Mojo-Webqq-IPwhere 

Mojo-Webqq IP归属地查询插件，用本插件可以在qq群（微信群）
交互式查询IP归属地。

##关于Mojo-Webqq：

[github]（https://github.com/sjdy521/Mojo-Webqq）
[码云]（https://git.oschina.net/sjdy521/Mojo-Webqq）
QQ官方群 Mojo-Webqq 49888015

目前最强大的第三方qq客户端，可以支持qq挂机，多种插件机器人等。
使用Perl语言编写的Smartqq客户端框架，基于Mojolicious，要求Perl版本5.10+，可通过插件提供基于HTTP协议的api接口供其他语言或系统调用。

##更多

本插件需要，安装模块IP::IPwhere，如果你需要纯真的信息
还要安装IP::QQWry，以及下载纯真的数据库QQWry.Dat
下载地址:
     https://github.com/bollwarm/ipwhere/blob/master/QQWry.Dat
安装库可以简单通过cpanm IP::IPwhere IP::QQWry
并把下面部分注释去掉。

    my $qqwry = IP::QQWry->new('QQWry.Dat');

    sub gquery {
    my ($ip)=shift;
    my ($base,$info) = $qqwry->query($ip);
    my $result;
    $result="qqwry $ip:";
    $result.=decode('gbk',$base);
    $result.=decode('gbk',$info)."\n";
    return $result;
    }

